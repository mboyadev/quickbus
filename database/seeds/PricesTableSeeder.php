<?php

use Illuminate\Database\Seeder;
use App\Models\Price;
use App\Models\Town;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Price::truncate();
        $towns =Town::get();

        $towns->each(function($town) use($towns) {
            
            $towns->each(function($town1) use($town){
                //if(!Price::search($town->id,$town1->id)){

                    $price = new Price;
                    $price->price_town_from_id = $town->id;
                    $price->price_town_to_id =$town1->id; 
                    $price->price_amount = $this->randPrice();        
                    $price->save();

                    echo ".";
               // }
              
              
            });

    	});

    }

    public function randPrice(){
        $min=7;
        $max=22;
        return (mt_rand($min,$max) * 100);
    }

}
