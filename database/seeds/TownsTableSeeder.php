<?php

use Illuminate\Database\Seeder;
use App\Models\Town;

class TownsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Town::truncate();

         $towns=array("Mombasa","Nairobi","Nakuru","Kisumu","Kampala","Voi","Kitale",
         "Malindi","Tanga","Dar-es-Salaam","MtitoAndei","Eldoret","Bungoma","Kericho",
         "Malaba","Kisii","Horohoro","Busia","Webuye","Migori");

        foreach($towns as $town_name){
            $new_town = new Town;
            $new_town->town_name = $town_name;
            $new_town->town_code = '';
    		$new_town->save();

    		echo ".";
        }
       
    }
}
