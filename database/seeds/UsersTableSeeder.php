<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $user=User::create([
            'user_first_name' => 'QuickBus',
            'user_last_name' => 'QuickBus',
            'user_email' => 'admin@quickbus.com',
            'user_username' => 'quickbus',
            'password' => bcrypt('12345678')
       
       
        ]);

        $user=User::create([
            'user_first_name' => 'dan',
            'user_last_name' => 'mboya',
            'user_email' => 'dan@mboya.com',
            'user_username' => 'dan',
            'password' => bcrypt('12345678')
        
        ]);

    }
}
