<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');  
            $table->integer('amount');    
            $table->integer('town_from_id')->nullable()->default(null);  
            $table->integer('town_to_id')->nullable()->default(null);         
            $table->boolean('is_active')->default(1)->comment('0 is no, 1 is yes');
            $table->boolean('is_used')->default(0)->comment('0 is no, 1 is yes');            
            $table->timestamp('expiry_date')->nullable()->default(null);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_codes');
    }
}
