<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'users';

    /**
     * Run the migrations.
     * @table users
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
            
        Schema::create($this->set_schema_table, function (Blueprint $table) {
          
            $table->bigIncrements('id');
            $table->string('user_first_name', 20);
            $table->string('user_last_name', 20);
            $table->string('user_phone_number', 20)->nullable()->default(null);
            $table->integer('user_id_number')->nullable()->default(null);
            $table->string('user_email', 30)->nullable()->default(null);
            $table->string('user_username', 20)->nullable()->default(null);
            $table->string('password', 100)->nullable()->default(null);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->tinyInteger('user_login_attempts')->default('0');
            $table->smallInteger('password_reset_code')->default(0); 
            $table->tinyInteger('user_is_suspended')->default('1')->comment('1=active, 0=not active');
            $table->integer('user_suspended_by')->default(0);
            $table->timestamp('user_suspended_date')->nullable()->default(null);
            $table->integer('user_activated_by')->default(0);
            $table->timestamp('user_activated_date')->nullable()->default(null);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
