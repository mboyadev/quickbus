<?php

Route::group(['prefix' => 'v1'], function() {
	Route::group(['middleware' => 'guest'], function() {
		Route::post('login', 'Internal\AuthController@login');
	});

	Route::group(['middleware' => ['auth:api'], 'namespace' => 'Internal'], function() {
	
		Route::post('promo-codes/{id}/activate', 'PromoCodesController@activatePromoCode');
		Route::post('promo-codes/{id}/deactivate', 'PromoCodesController@deactivatePromoCode');
		Route::resource('promo-codes', 'PromoCodesController');
		Route::post('booking/prices', 'BookingController@getDiscountedPrices');
		Route::resource('booking', 'BookingController');
		

		Route::get('init', 'AuthController@init');
		Route::get('logout', 'AuthController@logout');
		// Route::get('dashboard', 'DashboardController@index');

		Route::put('users/{id}/auth', 'UsersController@auth');
		Route::resource('users', 'UsersController');

		Route::resource('towns', 'TownsController');

		Route::resource('prices', 'PricesController');
		
	});
});

Route::any('{path?}', function() {
	return 'API working.';
});
