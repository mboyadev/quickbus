# QUICKBUS INSTALLATION MANUAL

### Installation ###

* `git clone https://mboyadev@bitbucket.org/mboyadev/quickbus.git projectname`
* `cd projectname`
* `composer install`
* `php artisan key:generate`
* Create a database and inform *.env* or .env.example* for my case database name is 'qb'
* `php artisan migrate --seed` to create and populate tables
* `php artisan passport:install` and inform *.env* or .env.example* of the generated password grant client id and secret key
* `php artisan vendor:publish` to publish filemanager
* `php artisan serve` to start the app on http://localhost:8000/

