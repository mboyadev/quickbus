# QUICKBUS API Manual

###### Important: Request Headers
```
Content-Type: application/json,
Origin: *,
Authorization: Bearer .“access_token”, eg,
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjJlMmI2N2E5ZmVlYTg5ZWUwNDNlMmZhNjEzMjk2MTIwZmYwODI0YTgwMTI3MWRjZDNhNTk0N2ZmYjA3NGQ1NzY2YmE4ODdjYWQ0MmY2YjJiIn0.eyJhdWQiOiIyIiwianRpIjoiMmUyYjY3YTlmZWVhODllZTA0M2UyZmE2MTMyOTYxMjBmZjA4MjRhODAxMjcxZGNkM2E1OTQ3ZmZiMDc0ZDU3NjZiYTg4N2NhZDQyZjZiMmIiLCJpYXQiOjE1MTgxNTk3NTMsIm5iZiI6MTUxODE1OTc1MywiZXhwIjoxNTQ5Njk1NzUzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.gkG9c31pzddOFb-CNNaaiwkYbOiaqLWisfNZburtAHm5qmv3x-cNYXSZ5IGhLj2
```


### 1. Authentication
###### Request
```json
{
    type: POST,
    url: /api/v1/login,
    data: {
        email: email,
        password: password
    }
}
```

###### Response
On success, returns
```json
{
  "token_type":"Bearer",
  "expires_in":31536000,
  "access_token":"access token",
  "refresh_token":"refresh token"
}
```


On failure
```json
{
  "error":"invalid_credentials",
  "message":"The user credentials were incorrect."
}
```

###### Important: Save the access token and attach it to each subsequent request as
```
Authorization: Bearer .“access_token”, eg,
Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjJlMmI2N2E5ZmVlYTg5ZWUwNDNlMmZhNjEzMjk2MTIwZmYwODI0YTgwMTI3MWRjZDNhNTk0N2ZmYjA3NGQ1NzY2YmE4ODdjYWQ0MmY2YjJiIn0.eyJhdWQiOiIyIiwianRpIjoiMmUyYjY3YTlmZWVhODllZTA0M2UyZmE2MTMyOTYxMjBmZjA4MjRhODAxMjcxZGNkM2E1OTQ3ZmZiMDc0ZDU3NjZiYTg4N2NhZDQyZjZiMmIiLCJpYXQiOjE1MTgxNTk3NTMsIm5iZiI6MTUxODE1OTc1MywiZXhwIjoxNTQ5Njk1NzUzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.gkG9c31pzddOFb-CNNaaiwkYbOiaqLWisfNZburtAHm5qmv3x-cNYXSZ5IGhLj2
```

#### Logout
##### Request 
```json
{
  'url': '/api/v1/logout',
  method: 'POST'
}
```
##### Response:
```json
{
    "success": true,
    "message": "You are Logged out."
}


```
###### Note: Delete the access_token at this time.
#
#
#### Authenticated User
##### Request
```json
{
  url: '/api/v1/init',
  method: 'GET'
}
```
##### Response 
```json
{
    "user": {
        "name": "QuickBus QuickBus",
        "email": "admin@quickbus.com",
        "id": 518,
        "department_id": 2
    },

}
```
##### Note
From now on, unauthenticated users will get response of status 401 with:
```json
{
    "message": "Unauthenticated."
}
```
Requests to non-existent resources will return a status 404 Not Found, eg, Fetching the status with a wrong ticket id will return a 404.
        

# Towns
## Create a towns - /api/v1/towns
Resourceful routes are index, store

## Index
```javascript
{
	url: '/api/v1/towns',
	
}
```

##### Response   
```json
[{id: 1, town_name: "Mombasa", town_code: ""}, {id: 2, town_name: "Nairobi", town_code: ""},…]
```

### store
```javascript
{
    'town_name': 'required',
    'town_code': 'optional'
}
```

# Promo Codes
## Create a Promo Code - /api/v1/promo-codes
Resourceful routes are index, create, store

## Index
```javascript
{
	url: '/api/v1/promo-codes',
	optional_params: [
		'town_from_id',
		'town_to_id',
		'is_active',
		'is_used',
		'amount',
	]
}

```

##### Response   
```json
{total: 4, page: 1, per_page: "25", 
results: [
{id: 4, code: "PBNQSGHR", amount: 100, is_used: 1, is_active: 1, expiry_date: "2018-12-18 13:13:00",town_from:"Nairobi",town_to:"Mombasa"…},…]}

```

### create
```javascript
{
    "towns": [
        "id",
        "town_name"
    ]
}
```

### store
```javascript
{
     'amount' : 'required|integer',
     'town_to_id' : 'required|integer',
      count' : 'required|integer',
     'expiry_date' : 'date_format:"Y-m-d H:i"|required',
     'town_from_id' : 'optional|integer',            
    
}
```

##### Response   
```json
results : {"success":1}
```

### activate - api/v1/promo-codes/{id}/activate   Method:POST
```javascript

```
### deactivate - api/v1/promo-codes/{id}/deactivate  Method:POST
```javascript

```



# Booking

### /api/v1/booking/prices  Method:POST

Testing the promo code to see if trips are discounted for valid promo code.
Normal prices are returned if no promo code is provied.
NB: A promo code can only be used once/by one customer after which it is invalid
Fields required are 
```php
{
    'town_from' : 'required|string',  ~Nairobi
    'town_to' : 'required|string',   ~Mombasa
    'promo_code' : 'optional' ~ Validated if provided
 }
 
```

##### Response   
```json
 {
 discounted_price:1000,
 promo_code: {id: 3, code: "PQ2HONXX", amount: 100, is_used: 1, is_active: 1, expiry_date: "2018-12-18 13:13:00",town_from:"Nairobi",town_to:"Mombasa"…}
}
```


