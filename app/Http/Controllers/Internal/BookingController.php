<?php

namespace App\Http\Controllers\Internal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PromoCode;
use App\Models\Town;
use App\Models\Price;
use DB;
use Carbon\Carbon;

class BookingController extends Controller
{  
    public function __construct()
    {     parent::__construct();
        $this->middleware('paginator', ['only' => ['index']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return [
            'towns' => Town::get(['id','town_name'])  
        ];
    }

 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
      return DB::transaction(function() use ($request) {
        return ['success' => 1];

      });

    }


    public function getDiscountedPrices(Request $request)
    { 
        $request->validate([    
            'town_from' => 'required',
            'town_to' => 'required'
        ]);

        // Getting town from and town to details;
        $townFrom=Town::where('town_name','like', $request->get('town_from'))->firstOrFail();
        $townTo=Town::where('town_name','like', $request->get('town_to'))->firstOrFail();
        $normalRate=Price::getRoutePrice($townFrom->id,$townTo->id);

        if($request->has('promo_code')){
            if($townTo->id !=1){
                abort(403, "Invalid promo code for destination town, ".$townTo->town_name);
            }
            else{
              $now=Carbon::now()->format('Y-m-d H:i:s');
                         
              $promoCode=PromoCode::where('code', $request->get('promo_code'))
                                    ->where('is_active',1)->where('is_used',0)
                                    ->where('town_to_id',$townTo->id)
                                    ->where('expiry_date','>',$now)
                                    ->first();
              if($promoCode){
                $promoCode->update(['is_used' => 1]);
                return [
                    'discounted_price' => $normalRate > $promoCode->amount ? 
                    $normalRate - $promoCode->amount : 0,
                    'promo_code' =>  $promoCode->parse()
                     ];

              }
              abort(403, "Invalid promo code.");

            }           

        }
        else{
        
            return [
            'discounted_price' => $normalRate
             ];

        }

    }
}
