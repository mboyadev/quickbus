<?php

namespace App\Http\Controllers\Internal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PromoCode;
use App\Models\Town;
use App\Models\Price;
use App\Repos\GlobalFunctions;
use DB;
use Carbon\Carbon;

class PromoCodesController extends Controller
{  
    public function __construct()
    {     parent::__construct();
        $this->middleware('paginator', ['only' => ['index']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         $data = [];
        if($request->has('amount')){
            $data['amount'] = $request->get('amount');
        }
        if($request->has('town_from_id')){
            $data['town_from_id'] = $request->get('town_from_id');
        }
        if($request->has('town_to_id')){
            $data['town_to_id'] = $request->get('town_to_id');
        }

        if($request->has('is_used')){
            $data['is_used'] = $request->get('is_used');
        }

        if($request->has('is_active')){
            $data['is_active'] = $request->get('is_active');
        }

        $results= PromoCode::filter($data)->paginate($request->show);

        return [
            'total' => $results->total(),
            'page' => $results->currentPage(),
            'per_page' => $results->perPage(),
            'results' => $results->map(function($row) {
                return $row->parse();
            })
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return [
            'towns' => Town::get(['id','town_name'])
        ];
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
       return DB::transaction(function() use ($request) {
       $this->validateRequest($request);
       
     
       $data=$request->all();        
       
       for($i=0; $i<$request->count; $i++){

        $data['code']=PromoCode::generatePromoCode();
        $data['expiry_date']=Carbon::parse($request->expiry_date)->format('Y-m-d H:i');//date('H:m',strtotime($day));
        $promoCode = PromoCode::create($data);
       }  
      
       return ['success' => 1];

    });

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return PromoCode::getAll($id)->parse();
    }

   
    public function activatePromoCode(Request $request, $id)
    {    
       return DB::transaction(function() use ($request,$id) {      
    
       $promoCode = PromoCode::findOrFail($id);        
       $data['is_active']=1;
       $promoCode->update($data);
      
        return ['success' =>1];
    });
    }

    public function deactivatePromoCode(Request $request, $id)
    {   
       return DB::transaction(function() use ($request,$id) {      
    
       $promoCode = PromoCode::findOrFail($id);        
       $data['is_active']=0;
       $promoCode->update($data);      
        return ['success' =>1];
     });
    }

   
    public function validateRequest(Request  $request){
        $request->validate([
            
            'amount' => 'required|integer',
            'town_to_id' => 'required|integer',
            'count' => 'required|integer',
            'expiry_date' => 'date_format:"Y-m-d H:i"|required',       
            

        ]);
    }

}
