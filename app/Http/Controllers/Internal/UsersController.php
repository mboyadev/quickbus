<?php

namespace App\Http\Controllers\Internal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Validation\Rule;


class UsersController extends Controller
{
    public function __construct()
    {    parent::__construct();
         $this->middleware('paginator', ['only' => ['index']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $results = User::paginate($request->show);

        return [
            'total' => $results->total(),
            'page' => $results->currentPage(),
            'per_page' => $results->perPage(),
            'results' => $results->map(function($row) {
                return $row->parse();
            })
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return [ 
            
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_first_name' => 'required',
            'user_last_name' => 'required',
            'user_phone_number' => 'required',
            'user_id_number' => 'required',
            'user_email' => 'required|unique:users,user_email',
            'user_username' => 'required|unique:users,user_username'
           
        ]);

        $data = $request->all();
        $password = bcrypt($data['user_email']);
        $data['password'] = $password;

        $user = User::create($data);
      
        return $user->parse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::getAll($id)->parse();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->create();
        $data['user'] = $this->show($id);

        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'user_first_name' => 'required',
            'user_last_name' => 'required',
            'user_phone_number' => 'required',
            'user_id_number' => 'required',
            'user_email' => 'required',
            'user_username' => 'required'
          
        ]);

        $user = User::getAll($id);

        $user->update($request->all());
    
        return $user->parse();
    }

    public function auth(Request $request, $id)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required|confirmed'
        ]);

        $user = User::getAll($id);

        $data = [
            'username' => $request->username,
            'password' => bcrypt($request->password)
        ];

        $user->update($data);

        return [
            'success' => 1
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $model=User::findOrFail($id);
        $model->delete();
       
        return [
            'success' => 1
        ];
    }


}
