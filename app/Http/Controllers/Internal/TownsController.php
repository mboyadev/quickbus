<?php

namespace App\Http\Controllers\Internal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Town;


class TownsController extends Controller
{
    public function __construct()
    {   parent::__construct();
      
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $results = Town::get();

        return 
                $results->map(function($row) {
                return $row->parse();
            })
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Country::all(['id', 'country_name']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'town_name' => 'required|max:50',
            'town_code' => 'required|max:10'
           
            
        ]);
       
        $data=$request->all();
    
        $town = Town::create($data);
        return $town->parse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $town = Town::getAll($id);        
        return $town->parse();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $town = Town::findOrFail($id);

        $request->validate([
            'town_name' => 'required|max:50',
            'town_code' => 'required|max:10'
        ]);


        $town->update($request->all());

        $town = Town::getAll($id);

        return $town->parse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $model=Town::findOrFail($id);
        $model->delete();
    
        return [
            'success' => 1
        ];
    }
}
