<?php

namespace App\Http\Controllers\Internal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use DB;
use Route;


class AuthController extends Controller
{
    public function login(Request $request)
    {
      

        $request->validate([
    		'username' => 'required',
    		'password' => 'required'
    	]);

    	$user = User::where('user_email', '=', $request->username)
    		->orWhere('user_username', '=', $request->username)
    		->first();

        if ($user == null) {
            $msg = [
                "error" => "invalid_credentials",
                "message" => "The user credentials were incorrect." 
            ];

            abort(401, 'The user credentials were incorrect', $msg);
        } 

        return $this->newLogin($request,$user);
    	
    }

    private function newLogin(Request $request,$user)
    {
    	$new_request = Request::create('oauth/token', 'POST', [
            'client_id' => env('PASSWORD_GRANT_CLIENT_ID'),
            'client_secret' => env('PASSWORD_GRANT_CLIENT_SECRET'),
            'username' => $request->username,
            'password' => $request->password,
            'grant_type' => 'password',
            'provider' => 'api',
            'scope' => '*'
        ]);

        $new_request->headers->set('Origin', '*');
 
        $data= app()->handle($new_request);
    
        return $data;
    }

   
    
    public function init()
    {
        return [
            'auth_user' => auth_user()
        ];
    }

    public function getUserId(Request $request)
    {   
        return auth_user();
      
    }

    public function logout(Request $request)
    {
    	$request->user()->token()->revoke();

        return [
            'success' => 1,
        ];
    
    }


}
