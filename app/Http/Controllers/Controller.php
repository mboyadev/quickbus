<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $user;

    function __construct()
    {
    	$this->user = (object) auth_user()->toArray();
    }

    public function authorize($permission)
    {
    	if (collect($this->user->permissions)->has($permission) == false) {
    		abort(401, 'You are not allowed to perform this action');
    	}
    }

}
