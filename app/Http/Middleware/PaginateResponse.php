<?php

namespace App\Http\Middleware;

use Closure;
use Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class PaginateResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->get('show') < 5) {
            $request->request->add(['show' => 30]);
        }

        if ($request->get('page') < 1) {
            $request->request->add(['page' => 1]);
        }

        return $next($request);
    }
}
