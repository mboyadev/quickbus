<?php

function parse_phone_number($number)
{
	$country_code = (int) substr($number, 0, 3);

	if ($country_code != 254 && $country_code != 255 && $country_code != 256 && $country_code != 250) {
		abort(422, 'Invalid Phone Number');
	}

	return $number;
}

function auth_user()
{
	if (Auth::guard('api')->check()) {
		return App\Models\User::getAll(Auth::guard('api')->id())->parse();
	}

	return collect([]);
}