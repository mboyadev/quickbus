<?php

namespace App\Models;

use App\Models\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Town extends Eloquent
{
	use SoftDeletes;
	
	protected $connection = 'mysql';
	protected $primaryKey = 'id';
	protected $perPage = 30;
	public $timestamps = false;

	protected $fillable = [
		'town_name','town_code',
		'town_status'
	];


	public static function getAll($id = null)
	{
		if ($id) {
			return self::findOrFail($id);
		}

		return self::get();
	}

	public function parse()
	{
		return [
            'id' => $this->id,
			'town_name' => $this->town_name,
			'town_code' => $this->town_code
		];
	}
}
