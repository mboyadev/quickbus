<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Roksta\Punctuator\Spacer;

class User extends Authenticatable
{
	use Notifiable, HasApiTokens, Spacer, SoftDeletes;

	protected $connection = 'mysql';
	protected $perPage = 30;
	public $timestamps = false;

	protected $casts = [
		'user_id_number' => 'int',		
        'user_is_suspended'=>'int'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'user_first_name',
        'user_last_name',
		'user_phone_number',
		'user_id_number',
		'user_email',
		'user_username',
		'password',
        'user_login_attempts',
        'password_reset_code',
        'user_is_suspended'
	];

   
    public function getName()
	{
		return $this->user_first_name . ' ' . $this->user_last_name;
	}
    
	public function findForPassport($identifier) {
        return $this->orWhere('user_email', $identifier)
            ->orWhere('user_username', $identifier)
            ->first();
    }

    public function setPunctuateColumns(): Array
    {
        return ['short' => ['name'], 'long' => []];
    }

   

    
    public function parse()
    { 
    	return collect([
    		'id' => $this->id,
            'user_first_name' => $this->user_first_name,
            'user_last_name' => $this->user_last_name,
			'user_phone_number' => $this->user_phone_number,
			'user_id_number' => $this->user_id_number,
			'user_email' => $this->user_email,
            'user_username' => $this->user_username,
            'user_status' => $this->user_is_suspended==1? 'active' : 'suspended'
           
    	]);
    }

    public static function getAll($id = null)
    {
    

        if ($id) {
            return self::findOrFail($id);
        }

        return self::get();
    }


}
