<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class PromoCode extends Model
{
    use SoftDeletes;

	protected $connection = 'mysql';
	protected $perPage = 30;
	public $timestamps = false;

	protected $casts = [
		
	];


	protected $fillable = [
		'code',
		'amount',
		'town_from_id',
		'town_to_id',
		'is_used',
		'is_active',		
		'expiry_date'
		

	];


	public static function generatePromoCode()
	{
		$number = "P".strtoupper(str_random(7));

		$already_used = self::where('code', '=', $number)->count();

		if ($already_used == 0) {
			return $number;
		}

		return self::generatePromoCode();
	}

	public function townFrom()
	{
		return $this->belongsTo(Town::Class, 'town_from_id');
	}

	public function townTo()
	{
		return $this->belongsTo(Town::Class, 'town_to_id');
	}

	public static function getAll($id = null)
	{
		if ($id) {
			return self::with(['townFrom','townTo'])->findOrFail($id);
		}

		return self::with(['townFrom','townTo'])->get();
	}

	public function parse()
	{
		return [
			'id' => $this->id,
			'code' => $this->code,
			'amount' => $this->amount,			
			'is_used' => $this->is_used,	
			'is_active' => $this->is_active,		
			'expiry_date'=>$this->expiry_date,
			'town_from' =>	$this->townFrom ?  $this->townFrom->town_name
               	 : null,
			'town_to' => $this->townTo ? $this->townTo->town_name
               	 : null

			
		];
	}

	
public static function filter($data)
	{
		$query = self::with(['townFrom','townTo']);
		
		if (isset($data['town_from_id'])) {
			$query->where('town_from_id', '=', $data['town_from_id']);
		}

		if (isset($data['town_to_id'])) {
			$query->where('town_to_id', '=', $data['town_to_id']);
		}

		if (isset($data['amount'])) {
			$query->where('amount', '=', $data['amount']);
		}

		if (isset($data['is_used'])) {
			$query->where('is_used', '=', $data['is_used']);
		}

		if (isset($data['is_active'])) {
			$query->where('is_active', '=', $data['is_active']);
		}

		
        $query->orderByDesc('id');
		return $query;
	}
	

}
