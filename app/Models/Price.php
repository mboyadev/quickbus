<?php

namespace App\Models;

use App\Models\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Eloquent
{
	use SoftDeletes;
	
	protected $connection = 'mysql';
	protected $table = 'prices';
	protected $perPage = 30;
	public $timestamps = false;

	protected $casts = [
	
	];


	protected $fillable = [
	    'price_town_from_id',
		'price_town_to_id',            
		'price_amount'
		
	];
	
	public function townFrom()
	{
		return $this->belongsTo(Town::Class, 'price_town_from_id');
	}

	public function townTo()
	{
		return $this->belongsTo(Town::Class, 'price_town_to_id');
	}
	
	public static function getRoutePrice($town_from_id,$town_to_id){
		
		$price=Price::where('price_town_from_id',$town_from_id)
		->where('price_town_to_id',$town_to_id)->first();	

		if($price)
		return $price->price_amount;
	
		return 0;
		
   }


   public static function search($a, $b)
	{
		return self::where(function($query) use($a, $b) {
			return $query->where('price_town_from_id', '=', $a)
				->where('price_town_to_id', '=', $b);
		})->orWhere(function($query) use($a, $b) {
			return $query->where('price_town_from_id', '=', $b)
				->where('price_town_to_id', '=', $a);
		})->first();
	}

	public static function getAll($id = null)
	{
		if ($id) {
			return self::findOrFail($id);
		}

		return self::get();
	}

	public function parse()
	{
		return [
			'id' => $this->id,
			'price_amount' => $this->price_amount,			
			'price_town_from' =>	$this->townFrom ? [
				'id' => $this->townFrom->id,
				'name' => $this->townFrom->town_name
               	] : null,
			'price_town_to' => $this->townTo ? [
				'id' => $this->townTo->id,
				'name' => $this->townTo->town_name
               	] : null

		];
	}
	
	
	public static function filterPrices($data)
	{
		$query = self::with(['townFrom', 'townTo']);

		if ($data['town_from_id']) {
			$query->where('price_town_from_id', '=', $data['town_from_id']);
		}

		if ($data['town_from_to']) {
			$query->where('price_town_to_id', '=', $data['town_from_to']);
		}		
        $query->orderByDesc('id');
		return $query;
	}
}
